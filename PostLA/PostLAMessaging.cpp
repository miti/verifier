//
// Created by miti on 2019-12-24.
//

#include "PostLAMessaging.h"
#include "sgx_trts.h" // for sgx_read_rand
#include "crypto.h" // for aes_gcm_128
#include <unistd.h>
#include <stdio.h>
uint32_t PostLAMessaging::aes_gcm_wrapper(int enc, uint8_t* plaintext, uint32_t plaintext_length, uint8_t* ciphertext, uint32_t* ciphertext_length)
{
    uint32_t actual_plaintext_length=plaintext_length;
    uint8_t tag[16];uint32_t counter, return_status;
    uint8_t iv[12];
    if(enc == 0)
    {
        for(counter=0;counter<16;counter++)
            tag[counter]=plaintext[counter+plaintext_length-16];
        for(counter=0;counter<12;counter++)
            iv[counter]=plaintext[counter+plaintext_length-28];
        actual_plaintext_length-=28;
    }
    else
    {
        return_status=sgx_read_rand(iv, 12);
        if(return_status != 0)
            return return_status;
    }
    return_status = aes_gcm_128(enc, key, iv, plaintext, actual_plaintext_length, ciphertext, ciphertext_length, tag);
    if(enc == 1 && return_status == 0)
    {
        for(counter=0;counter<12;counter++)
            ciphertext[counter + *ciphertext_length] = iv[counter];
        for(counter=0;counter<16;counter++)
            ciphertext[counter + 12 + *ciphertext_length] = tag[counter];
        *ciphertext_length=*ciphertext_length + 28;
    }
    return return_status;
}

// The verifier doesn't receive any messages (in the deployment stage or at all)
uint32_t PostLAMessaging::send_secure_msg(uint8_t* input, uint32_t input_size)
{
    uint8_t* output;
    uint32_t output_size, ret;

    output = (unsigned char*) malloc(input_size + 28); // 16 for tag, 12 for IV
    ret = aes_gcm_wrapper(1, input, input_size, output, &output_size );
    if(ret != 0)
        return ret;

    size_t post_la_bytes_written = write(fd, output, output_size);
    printf("Wrote the hash and the tag to the decryptor socket.\n Wrote this many bytes: %d\n", post_la_bytes_written); fflush(stdout);

    if(close(fd)!= 0)
    {
        printf("Error in closing the socket connection.\n"); fflush(stdout); return 0xfd;
    }

    // TODO: Conversion logic to protobuf. Set msg or whatever.
    /*  google::protobuf::MessageLite protobuf_msg;
     * if(protobufReaderWriter->write_msg(protobuf_msg) != 0)
        return 0x3;
    */
    return 0;
}

void PostLAMessaging::set_la_symmetric_key(uint8_t* given_key) {
    uint32_t counter;
    for(counter=0; counter<16; counter++)
    {
        key[counter] = given_key[counter];
    }
}

void PostLAMessaging::set_fd(int given_fd)
{
    // protobufReaderWriter.set_fd(given_fd);
    fd = given_fd;
}

/*
int encrypt_decrypt_msgs(int encrypt_decrypt, std::vector<std::string> &input_msgs,
                         std::vector<std::string> &output_msgs)
{
    unsigned char *input; unsigned char *output;
    uint32_t input_size, output_size, ret;
    output=NULL;

    for (std::string msg:input_msgs)
    {
        input_size = msg.length();
        input = (unsigned char*) msg.c_str();
        output = (unsigned char*) realloc(output, input_size + 28); // 16 for tag, 12 for IV
        ret = aes_gcm_wrapper(encrypt_decrypt, input, input_size, output, &output_size );
        if(ret!=0)
        {
            free(output);
            printf("Failed to encrypt an input field.\n"); fflush(stdout);
            return 0x2;
        }
        output_msgs.push_back(std::string(reinterpret_cast<const char *> (output), output_size));
    }
    free(output);

    return 0;
}
*/

/*
 * virtual void create_vector_from_protobuf(google::protobuf::MessageLite& protobuf_msg,
                                         std::vector<std::string> &native_msg_list) {}

uint32_t receive_secure_msgs(std::vector<std::string> &plaintext_msg_list) {
    std::vector<std::string> ciphertext_msg_list;
    google::protobuf::MessageLite protobuf_msg;

    // read encrypted data
    if(!protobufReaderWriter.read_msg(protobuf_msg))
    {
        printf("Not all of the decryptor's message was read\n"); fflush(stdout);
        return 0xf3;
    }

    create_vector_from_protobuf(protobuf_msg, ciphertext_msg_list);

    return encrypt_decrypt_ciphertexts(0, &ciphertext_msg_list, plaintext_msg_list);
}
*/

/*
uint32_t send_secure_msgs(std::vector<std::string> &plaintext_msg_list)
{
    uint32_t ret;
    std::vector<std::string> ciphertext_msg_list;
    google::protobuf::MessageLite protobuf_msg;

    ret=encrypt_decrypt_msgs(1, plaintext_msg_list, &ciphertext_msg_list);
    if(ret!=0)
        return ret;

    create_protobuf_from_vector(ciphertext_msg_list, protobuf_msg);

    // write message to decryptor
    if(!protobufReaderWriter.write_msg(protobuf_msg))
    {
        printf("Not all of the client's pub key and ciphertext data was written\n"); fflush(stdout);
        return 0xfe;
    }
    return 0;
}
*/