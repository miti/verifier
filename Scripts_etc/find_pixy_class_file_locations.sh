#!/bin/bash

locations=$(strace -ff -e trace=file /home/m2mazmud/good_code/Verifier/a.out ~/pixy-master/sample_mitigator/index.php 2>&1 | perl -ne 's/^[^"]+"(([^\\"]|\\[\\"nt])*)".*/$1/ && print' | grep ".class" | cut -d. -f1 | rev | cut -d/ -f 2- | rev | sort -u )
counter=0;
for x in $locations; 
do
	y=$(find $x -maxdepth 1 -type f | grep .class); 
	if [[ ! -z "$y" ]];
	then 
		let counter++;
		key=$(echo "$x" | rev | cut -d/ -f1 | rev); 
		echo "sgx.allowed_files.pixy_classes_"$key"_"$counter" = file:"$x; 
	fi;
done;

