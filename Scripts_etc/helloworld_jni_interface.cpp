#include <jni.h>

int main(int argc, char **argv)
{
    JavaVM         *vm;
    JNIEnv         *env;
    JavaVMInitArgs  vm_args;
    jint            res;
    jclass          cls;
    jmethodID       mid;
    jstring         jstr;
    jobjectArray    main_args;
    JavaVMOption* options = new JavaVMOption[5];
    options[0].optionString = "-Dpixy.home=/home/m2mazmud/pixy-master";
    options[1].optionString = "-Djava.class.path=/home/m2mazmud/pixy-master/lib:/home/m2mazmud/pixy-master/build/class";
    options[2].optionString = "-Xcheck:jni";
    options[3].optionString = "-Xms256m";
    options[4].optionString = "-Xmx1024m";
    vm_args.options            = options;
    vm_args.nOptions           = 5;


    vm_args.ignoreUnrecognized = JNI_TRUE;
    vm_args.version  = JNI_VERSION_1_8;
    if (JNI_CreateJavaVM(&vm, (void **)&env, &vm_args) != JNI_OK) {
        printf("Failed to create Java VMn");
        return 1;
    }

    cls = (env)->FindClass( "at/ac/tuwien/infosys/www/pixy/Checker");
    if (cls == NULL) {
        printf("Failed to find Main classn");
        return 1;
    }

    mid = (env)->GetStaticMethodID(cls, "main", "([Ljava/lang/String;)V");
    if (mid == NULL) {
        printf("Failed to find main functionnnnn");
        return 1;
    }
    printf("Hehehehe\n"); fflush(stdout);

    jclass classString = (env)->FindClass("java/lang/String");
    jstr      = (env)->NewStringUTF("");
    main_args = (env)->NewObjectArray( 8, classString, jstr);

    (env)->SetObjectArrayElement( main_args, 0, (env)->NewStringUTF( "-aAvLf"));
    (env)->SetObjectArrayElement( main_args, 1, (env)->NewStringUTF( "-s"));
    (env)->SetObjectArrayElement( main_args, 2, (env)->NewStringUTF( "/home/m2mazmud/pixy-master/config/sinks_xss.txt"));
    (env)->SetObjectArrayElement( main_args, 3, (env)->NewStringUTF( "-o"));
    (env)->SetObjectArrayElement( main_args, 4, (env)->NewStringUTF( "/home/m2mazmud/pixy-master/sample_mitigator/results"));
    (env)->SetObjectArrayElement( main_args, 5, (env)->NewStringUTF( "-y"));
    (env)->SetObjectArrayElement( main_args, 6, (env)->NewStringUTF( "xss"));
    if(argc>=1)
      (env)->SetObjectArrayElement( main_args, 7, (env)->NewStringUTF( argv[1]));

    (env)->CallStaticVoidMethod( cls, mid, main_args);
    if((env)->ExceptionOccurred()) {
	(env)->ExceptionDescribe(); 
      (env)->ExceptionClear();
      printf("Exception occurred\n"); fflush(stdout);
    }

    return 0;
}
