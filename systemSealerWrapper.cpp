#include <stdlib.h>
#include <unistd.h> // Read/write
#include <string>
#include <iostream>
#include <vector>
#include "SgxSealer.h"

//class SealerWrapper {

uint32_t seal_message_to_file(int fd, std::string& plaintext, uint32_t* actual_sealed_msg_length)
{
  uint32_t ret;  ssize_t bytes_written; std::string sgx_sealed_msg;
  ret = seal_message(plaintext, sgx_sealed_msg);
  if(ret!=0)
	return ret;

  bytes_written = write(fd, sgx_sealed_msg.c_str(), sgx_sealed_msg.length());
  if(bytes_written > 0)
  {
	fsync(fd);
	*actual_sealed_msg_length = bytes_written;
         return 0;
  }
  else
	return 0xFFFFFFFF;

}

// need to return a msg of the length returned by the get_encrypt_txt_len call.
uint32_t unseal_message_from_file(int fd, std::string& plaintext, uint32_t* expected_sealed_msg_length)
{
  uint32_t ret; ssize_t bytes_read; char* sgx_sealed_msg; std::string sgx_sealed_msg_str;
  sgx_sealed_msg = (char*) malloc(*expected_sealed_msg_length);
  lseek(fd, 0, SEEK_SET);
  bytes_read = read(fd, sgx_sealed_msg, *expected_sealed_msg_length);
  if(bytes_read <= 0)
  {
	free(sgx_sealed_msg);
	return 0xFFFFFFFF;
  }
  sgx_sealed_msg_str = std::string(sgx_sealed_msg, *expected_sealed_msg_length);
  free(sgx_sealed_msg);
  ret = unseal_and_verify_sealed_message(sgx_sealed_msg_str, plaintext);
  return ret;
}

//};
