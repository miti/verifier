#include "sgx.h"
#include "sgx_tseal.h"  // For sgx_seal_data, sgx_calc_sealed_data_size, sgx_get_encrypt_txt_len, sgx_unseal_data
#include <string>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <vector>



//class Sealer {
 uint32_t seal_message(std::string& plaintext_str, std::string& sgx_sealed_msg_str)
 {
   uint32_t ret;

   uint8_t* sgx_sealed_msg;
   uint32_t expected_sealed_msg_length;

   expected_sealed_msg_length = sgx_calc_sealed_data_size(0, 9);
   if(expected_sealed_msg_length == 0xFFFFFFFF)
       return 1;
   sgx_sealed_msg = (uint8_t*)malloc(expected_sealed_msg_length); // Doesn't change with protobufs - convert the data here to protobuf format after it is initialized

   ret = sgx_seal_data(0, NULL, 9, (uint8_t*) plaintext_str.c_str(), expected_sealed_msg_length, (sgx_sealed_data_t*) sgx_sealed_msg);
   sgx_sealed_msg_str = std::string((char*)sgx_sealed_msg, expected_sealed_msg_length); // TODO: Fishy conversion.
   free(sgx_sealed_msg);
   return ret;
 }

 uint32_t unseal_and_verify_sealed_message(std::string& sgx_sealed_msg_str, std::string& plaintext)
 {
    uint32_t ret = 0;
    uint8_t* sgx_sealed_msg;
    uint8_t* temp_plaintext;
    std::string protobuf_encoded_str; std::string decoded_plaintext;
    uint32_t sgx_counter_value;
    uint32_t expected_plaintext_msg_length;

    std::vector<uint8_t> sgx_sealed_msg_vector(sgx_sealed_msg_str.begin(), sgx_sealed_msg_str.end());// TODO: Add null termination?
    sgx_sealed_msg = &sgx_sealed_msg_vector[0];
    expected_plaintext_msg_length = sgx_get_encrypt_txt_len((sgx_sealed_data_t*)sgx_sealed_msg);
    if(expected_plaintext_msg_length == 0xffffffff)
      return 1;

    temp_plaintext = (uint8_t*)malloc( expected_plaintext_msg_length );
    ret = sgx_unseal_data((sgx_sealed_data_t*)sgx_sealed_msg, NULL, 0, temp_plaintext, &expected_plaintext_msg_length);
    if(ret != SGX_SUCCESS)
    {
	free(temp_plaintext);
        switch(ret)
        {
        case SGX_ERROR_MAC_MISMATCH:
            // MAC of the sealed data is incorrect.          The sealed data has been tampered.
            break;
        case SGX_ERROR_INVALID_ATTRIBUTE:
            // Indicates attribute field of the sealed data is incorrect.
            break;
        case SGX_ERROR_INVALID_ISVSVN:
            // Indicates isv_svn field of the sealed data is greater than            the enclave�s ISVSVN. This is a downgraded enclave.
            break;
        case SGX_ERROR_INVALID_CPUSVN:
            // Indicates cpu_svn field of the sealed data is greater than            the platform�s cpu_svn. enclave is  on a downgraded platform.
            break;
        case SGX_ERROR_INVALID_KEYNAME:
            // Indicates key_name field of the sealed data is incorrect.
            break;
        default:
            // other errors
            break;
        }
        return ret;
    }

    protobuf_encoded_str = std::string((char*)temp_plaintext, expected_plaintext_msg_length); // TODO: Fishy conversion.

    free(temp_plaintext);
    plaintext = protobuf_encoded_str;
    return ret;
  }

//}
