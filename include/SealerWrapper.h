    uint32_t seal_message_to_file(int fd, std::string& plaintext, uint32_t* actual_sealed_msg_size);
    uint32_t unseal_message_from_file(int fd, std::string& sealed_msg, uint32_t* expected_sealed_msg_size);
    uint32_t init();

