//#include "sgx_tae_service.h" // For sgx_sealed_data_t struct
//class Sealer {
//private: 
//    uint32_t verify_sgx_monotonic_counter_value(sgx_mc_uuid_t* sgx_monotonic_counter_id, uint32_t* sgx_monotonic_counter_value); 
//    uint32_t create_sgx_monotonic_counter_id_and_value(sgx_mc_uuid_t* sgx_counter_id, uint32_t* sgx_counter_value);
//public:
  uint32_t initialize_pse_and_counter();
  uint32_t seal_message(std::string& plaintext_str, std::string& sgx_sealed_msg);
  uint32_t unseal_and_verify_sealed_message(std::string& sgx_sealed_msg_str, std::string& plaintext);
//}
