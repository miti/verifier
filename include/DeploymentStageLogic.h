//
// Created by miti on 2019-12-24.
//

#ifndef VERIFIER_DEPLOYMENTSTAGELOGIC_H
#define VERIFIER_DEPLOYMENTSTAGELOGIC_H
#include "PostLAMessaging.h"
#include "LA.h"
#include <stdint.h>


class DeploymentStageLogic {
    LA laInitiator;
    PostLAMessaging postLaMessaging;
    uint8_t target_hash[32];

public:
    //	return_status=generate_rsa_keypair_hash(hash);
    //	if(return_status!=0)
    //		return return_status;

    void set_target_hash(uint8_t* given_hash);
    int set_up_socket_connect(int port);
    int main_logic(int fd);
};


#endif //VERIFIER_DEPLOYMENTSTAGELOGIC_H
