//
// Created by miti on 2019-12-24.
//

#ifndef VERIFIER_POSTLAMESSAGING_H
#define VERIFIER_POSTLAMESSAGING_H
#include <stdint.h>
//#include "crypto.h"
//#include "ProtobufMessageRW.h"
class PostLAMessaging {
    uint8_t key[16];
    int fd;
    // ProtobufMessageRW protobufReaderWriter;
    uint32_t aes_gcm_wrapper(int enc, uint8_t* plaintext, uint32_t plaintext_length, uint8_t* ciphertext, uint32_t* ciphertext_length);
public:
    void set_la_symmetric_key(uint8_t* given_key);
    void set_fd(int given_fd);
    uint32_t send_secure_msg(uint8_t* input, uint32_t input_size);
};


#endif
