#ifndef CRYPTO_H
#define CRYPTO_H
#include <stdio.h>
#include <string>

uint32_t generate_rsa_keypair(FILE* fp, std::string& priv_key_str, std::string& pub_key_str); //, uint8_t* hash);
uint32_t generate_rsa_keypair_hash(uint8_t* hash);
int aes_gcm_128(int enc, unsigned char *key, unsigned char *iv, unsigned char* plaintext, int plaintext_len, unsigned char *ciphertext,  uint32_t* op_ciphertext_len, unsigned char* tag);

#endif